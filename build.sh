ROOT=$PWD
SCRIPT_DIR=`dirname "$0"`
SCRIPT_DIR="$ROOT/$SCRIPT_DIR/"

if [ -f  /opt/linux/script/utils.lib ]
then
	echo "Build at Teamcity"
	cd /opt/linux/script
else
	echo "Build at Local"
	cd "$SCRIPT_DIR/../../script"
fi


. ./show_help.sh
. ./utils.lib
parse_opts "$@"


cd $SCRIPT_DIR

SysInfo_Exit_success

make 
SysInfo_Exit_success

make install
SysInfo_Exit_success
